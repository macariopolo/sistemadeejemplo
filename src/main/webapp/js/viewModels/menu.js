define([ 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'accUtils',
		'jquery' ], function(ko, app, moduleUtils, accUtils, $) {

	class MenuViewModel {
		constructor() {
			var self = this;

			self.userName = ko.observable();
			self.visibleCambioDePwd = ko.observable(false);
			self.pwdActual = ko.observable(null);
			self.pwd1 = ko.observable(null);
			self.pwd2 = ko.observable(null);
			self.error = ko.observable();

			self.photos = ko.observableArray([]);
						
			// Header Config
			self.headerConfig = ko.observable({
				'view' : [],
				'viewModel' : null
			});
			moduleUtils.createView({
				'viewPath' : 'views/header.html'
			}).then(function(view) {
				self.headerConfig({
					'view' : view,
					'viewModel' : app.getHeaderModel()
				})
			})
		}

		connected() {
			accUtils.announce('Menu page loaded.');
			document.title = "Menu";

			let self = this;

			let data = {
				url : "/user/getUserName",
				type : "get",
				success : function(response) {
					self.userName(response);
					self.getPhotos();
					self.error(null);
				},
				error : function(response) {
					self.userName(null);
					self.error(response.responseJSON.errorMessage);
				}
			}
			$.ajax(data);

		};

		mostrarFormulario() {
			this.visibleCambioDePwd(true);
		}

		cambiarPwd() {
			let self = this;
			let info = {
				pwd : this.pwdActual(),
				pwd1 : this.pwd1(),
				pwd2 : this.pwd2()
			};
			let data = {
				data : JSON.stringify(info),
				url : "user/cambiarPwd",
				type : "put",
				contentType : 'application/json',
				success : function(response) {
					alert("Tu contraseña se ha cambiado");
					self.visibleCambioDePwd(false);
				},
				error : function(response) {
					self.error(response.responseJSON.errorMessage);
				}
			};
			$.ajax(data);
		}

		getPhotos() {
			let self = this;
			let data = {
				url : "/photos/searchPicturesInGoogle?texto=" + self.userName(),
				type : "get",
				success : function(response) {
					for (let i=0; i<response.length; i++)
						self.getPhoto(response[i]);
				}
			}
			$.ajax(data);
		}

		getPhoto(src) {
			let self = this;

			let info = {
				url : src
			};
			let data = {
				url : "photos/getGooglePicture",
				type : "post",
				contentType : "application/json",
				data : JSON.stringify(info),
				success : function(response) {
					self.photos.push(response);
				}
			}
			$.ajax(data);
		}

		disconnected() {
			// Implement if needed
		};

		transitionCompleted() {
			// Implement if needed
		};
	}

	return MenuViewModel;
});
