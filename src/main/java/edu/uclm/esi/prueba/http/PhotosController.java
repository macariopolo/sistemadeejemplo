package edu.uclm.esi.prueba.http;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("photos")
public class PhotosController extends CookiesController {
	
	@GetMapping("/searchPicturesInGoogle")
	public List<String> searchPicturesInGoogle(@RequestParam String texto) throws Exception {
		CloseableHttpClient client = HttpClients.createDefault();
		texto = texto.replace(' ', '+');
		HttpGet get = new HttpGet("https://www.google.com/search?q=" + texto + "&tbm=isch");
		CloseableHttpResponse response = client.execute(get);
		HttpEntity entity = response.getEntity();
		String html = EntityUtils.toString(entity);
		
		client.close();
		
		List<String> result = new ArrayList<>();
		Document doc = Jsoup.parse(html);
		Elements imgs = doc.getElementsByTag("img");
		for (int i=0; i<imgs.size(); i++) {
			Element img = imgs.get(i);
			String src = img.attr("src");
			result.add(src);
		}
		return result;
	}
	
	@PostMapping("/getGooglePicture")
	public String getGooglePicture(@RequestBody Map<String, String> info) throws Exception {
		String url = info.get("url").toString();
		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet get = new HttpGet(url);
		CloseableHttpResponse response = client.execute(get);
		HttpEntity entity = response.getEntity();
		byte[] image = EntityUtils.toByteArray(entity);
		client.close();
		String picture = "data:image/png;base64," + Base64.getEncoder().encodeToString(image);
		return picture;
	}
}
