package edu.uclm.esi.prueba.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.uclm.esi.prueba.model.Login;

public interface LoginRepository extends JpaRepository <Login, String> {

}
