package edu.uclm.esi.prueba.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.uclm.esi.prueba.model.Token;

public interface TokenRepository extends JpaRepository<Token, String> {

}
